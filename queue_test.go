package sequentialpublisher

import (
	"errors"
	"testing"
	"time"
)

func generateSimpleError(err error) func() error {
	return func() error {
		return err
	}
}

var errorResponseQueue []func() error

func addResponseError(err error) {
	f := generateSimpleError(err)
	errorResponseQueue = append(errorResponseQueue, f)
}

func popResponseError() error {
	var err error
	if len(errorResponseQueue) > 0 {
		f := errorResponseQueue[0]
		err = f()
		errorResponseQueue = errorResponseQueue[1:]
	}
	return err
}
func resetResponseError() {
	errorResponseQueue = []func() error{}
}

type TestPublisher struct {
	Count int
}

func (t *TestPublisher) Publish(_ map[string]interface{}) error {
	t.Count += 1
	return popResponseError()
}

func TestPublisher_Run(t1 *testing.T) {
	errorResponseQueue = append(errorResponseQueue, nil)
	t1.Run("basic send success", func(t1 *testing.T) {
		c := make(chan interface{}, 25)
		p := TestPublisher{}

		t := NewQueue(c, &p, 1*time.Second)

		t.Run()

		if t.QueueSize() != 0 {
			t1.Error("Expected zero q size")
			return
		}

		// successfully sent
		resetResponseError()
		addResponseError(nil)

		c <- map[string]interface{}{"test": "test"}

		// wait
		time.Sleep(100 * time.Millisecond)
		if p.Count != 1 {
			t1.Error("Expect exactly one call to publish")
			return
		}
		if t.QueueSize() != 0 {
			t1.Error("Expected q of size 0")
			return
		}

	})

	t1.Run("q flushed with timer", func(t1 *testing.T) {
		c := make(chan interface{}, 25)
		p := TestPublisher{}

		t := NewQueue(c, &p, 1*time.Second)

		t.Run()

		if t.QueueSize() != 0 {
			t1.Error("Expected zero q size")
			return
		}

		// Fail to send
		resetResponseError()
		addResponseError(errors.New("failed to send"))

		c <- map[string]interface{}{"test": "test"}

		// wait
		time.Sleep(100 * time.Millisecond)
		if t.QueueSize() != 1 {
			t1.Error("Expected q of size 1")
		}
		if p.Count != 1 {
			t1.Error("Expect exactly one call to publish")
			return
		}

		// Successful resend (via timer)
		addResponseError(nil)

		// wait
		time.Sleep(1 * time.Second)
		if t.QueueSize() != 0 {
			t1.Error("Expected q of size 0")
		}
		if p.Count != 2 {
			t1.Error("Expect exactly 2 total calls to publish")
			return
		}

	})

	t1.Run("q partially flushed with timer", func(t1 *testing.T) {
		c := make(chan interface{}, 25)
		p := TestPublisher{}

		t := NewQueue(c, &p, 1*time.Second)

		t.Run()

		if t.QueueSize() != 0 {
			t1.Error("Expected zero q size")
			return
		}

		// Fail to send
		resetResponseError()
		addResponseError(errors.New("failed to send"))
		addResponseError(errors.New("failed to send"))
		addResponseError(errors.New("failed to send"))
		addResponseError(errors.New("failed to send"))

		c <- map[string]interface{}{"test": "test"}
		c <- map[string]interface{}{"test": "test"}
		c <- map[string]interface{}{"test": "test"}
		c <- map[string]interface{}{"test": "test"}

		// wait
		time.Sleep(100 * time.Millisecond)
		if t.QueueSize() != 4 {
			t1.Error("Expected q of size 4")
		}
		if p.Count != 4 {
			t1.Error("Expect exactly 4 call to publish")
			return
		}

		// Some successful resend (via timer)
		addResponseError(nil)
		addResponseError(nil)
		addResponseError(errors.New("failed to send"))

		// wait
		time.Sleep(1 * time.Second)
		if t.QueueSize() != 2 {
			t1.Error("Expected q of size 2")
		}
		if p.Count != 7 {
			t1.Error("Expect exactly 7 total calls to publish")
			return
		}

	})

	t1.Run("q flushed by next trade", func(t1 *testing.T) {
		c := make(chan interface{}, 25)
		p := TestPublisher{}

		t := NewQueue(c, &p, 1*time.Second)

		t.Run()

		if t.QueueSize() != 0 {
			t1.Error("Expected zero q size")
			return
		}

		// Fail to send
		resetResponseError()
		addResponseError(errors.New("failed to send"))

		c <- map[string]interface{}{"test": "test"}

		// wait
		time.Sleep(100 * time.Millisecond)
		if t.QueueSize() != 1 {
			t1.Error("Expected q of size 1")
		}

		// Successful resend (via timer)
		addResponseError(nil)
		addResponseError(nil)

		c <- map[string]interface{}{"test": "test"}
		// wait
		time.Sleep(100 * time.Millisecond)
		if t.QueueSize() != 0 {
			t1.Error("Expected q of size 0")
		}

	})

}
