# Sequential Publisher

Publisher acquires information from a go channel from one or more sources. The publisher is guaranteed to publish in sequential order. If failure, the publisher will block and re-attempt failure.