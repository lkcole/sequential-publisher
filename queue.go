package sequentialpublisher

import (
	"container/list"
	"log"
	"time"
)

type Publisher interface {
	Publish(map[string]interface{}) error
}

type Queue struct {
	ticker      *time.Ticker
	q           list.List
	msg         <-chan interface{}
	destination Publisher
}

func NewQueue(c <-chan interface{}, destination Publisher, flushTime time.Duration) *Queue {

	return &Queue{ticker: time.NewTicker(flushTime), destination: destination, msg: c}
}

func (t *Queue) Run() {

	go func() {
		for {
			select {
			case trade := <-t.msg:
				t.q.PushBack(trade)
			case <-t.ticker.C:
			}

			t.flushQueue()
		}
	}()
}

func (t *Queue) QueueSize() int {
	return t.q.Len()
}

func (t *Queue) flushQueue() {

	if t.q.Len() == 0 {
		return
	}

	processed := 0

	for t.q.Len() > 0 {
		front := t.q.Front()

		if err := t.destination.Publish(front.Value.(map[string]interface{})); err != nil {
			log.Printf("ERROR '%v', the following message queued (%v total messages remain), will attempt to process later: %+v", err, t.q.Len(), front.Value)
			break
		}

		t.q.Remove(front)

		processed += 1
	}

	if t.q.Len() == 0 {
		log.Printf("INFO queue successfully flushed and sent to destination (%d messages).\n", processed)
	}

	return
}
